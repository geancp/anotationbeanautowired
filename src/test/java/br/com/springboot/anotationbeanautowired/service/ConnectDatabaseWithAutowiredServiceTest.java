package br.com.springboot.anotationbeanautowired.service;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import br.com.springboot.anotationbeanautowired.systemparameter.DatasourceConfiguration;

@SpringBootTest
public class ConnectDatabaseWithAutowiredServiceTest {

    @Autowired
    private ConnectDatabaseWithAutowiredService connectDatabaseWithAutowiredService;

    
    @Test
    public void testDatasourceConfigurationInjection() {
        assertNotNull(connectDatabaseWithAutowiredService.getDatasourceConfiguration());
    }

    
    @Test
    public void testDatasourceConfigurationIsCorrect() {
    	DatasourceConfiguration datasourceConfiguration = connectDatabaseWithAutowiredService.getDatasourceConfiguration();
    	
		assertTrue("com.mysql.jdbc.Driver".equals(datasourceConfiguration.getDriverClassName()));
		assertTrue("jdbc:mysql://localhost:3306/mydb".equals(datasourceConfiguration.getUrl()));
		assertTrue("user".equals(datasourceConfiguration.getUsername()));
		assertTrue("password".equals(datasourceConfiguration.getPassword()));
    }
}
