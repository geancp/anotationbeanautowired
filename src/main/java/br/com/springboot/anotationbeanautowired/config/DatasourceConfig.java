package br.com.springboot.anotationbeanautowired.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import br.com.springboot.anotationbeanautowired.systemparameter.DatasourceConfiguration;

@Configuration
public class DatasourceConfig {

	@Bean
	public DatasourceConfiguration createDatasourceConfiguration() {

		DatasourceConfiguration datasourceConfiguration = new DatasourceConfiguration();
		
		datasourceConfiguration.setDriverClassName("com.mysql.jdbc.Driver");
		datasourceConfiguration.setUrl("jdbc:mysql://localhost:3306/mydb");
		datasourceConfiguration.setUsername("user");
		datasourceConfiguration.setPassword("password");
		
        return datasourceConfiguration;
	}
}
