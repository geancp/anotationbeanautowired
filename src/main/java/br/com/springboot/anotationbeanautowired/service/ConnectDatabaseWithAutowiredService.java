package br.com.springboot.anotationbeanautowired.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.springboot.anotationbeanautowired.systemparameter.DatasourceConfiguration;

@Service
public class ConnectDatabaseWithAutowiredService {

	@Autowired
    private DatasourceConfiguration datasourceConfiguration;

	
	public DatasourceConfiguration getDatasourceConfiguration() {
		return datasourceConfiguration;
	}
	
	// outros métodos da classe
}
