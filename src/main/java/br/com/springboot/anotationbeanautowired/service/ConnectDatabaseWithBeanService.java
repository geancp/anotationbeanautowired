package br.com.springboot.anotationbeanautowired.service;

import org.springframework.stereotype.Service;

import br.com.springboot.anotationbeanautowired.systemparameter.DatasourceConfiguration;

@Service
public class ConnectDatabaseWithBeanService {

	private DatasourceConfiguration datasourceConfiguration;

	
	public ConnectDatabaseWithBeanService(DatasourceConfiguration datasourceConfiguration) {
		this.datasourceConfiguration = datasourceConfiguration;
	}

	
	public DatasourceConfiguration getDatasourceConfiguration() {
		return datasourceConfiguration;
	}

	// outros métodos da classe
}
